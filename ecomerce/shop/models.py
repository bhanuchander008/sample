from django.db import models

# Create your models here.
class User(models.Model):
    name = models.CharField(max_length=30)
    adress =  models.CharField(max_length=50)
    phone = models.CharField(max_length=12)
    email = models.EmailField()
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
        return self.name



class Categories(models.Model):
    name =  models.CharField(max_length=30)
    status = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)

class Item(models.Model):
    name =  models.CharField(max_length=30)
    Descrption = models.CharField(max_length=100)
    price =models.DecimalField(max_digits=10, decimal_places=2)
    Categories = models.ForeignKey(Categories,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
        return self.name


class Order(models.Model):
      item= models.ForeignKey(Item,on_delete=models.CASCADE)
      user = models.ForeignKey(User,on_delete=models.CASCADE)
      status = models.BooleanField()
      Total_price = models.IntegerField(null=True)
      created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
      updated_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)


      def __str__(self):
        return self.item

# Create your models here.
