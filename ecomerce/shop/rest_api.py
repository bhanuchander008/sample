from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render
from django.core import serializers
from .serializers import UserSerializer,CategoriesSerializer,ItemSerializer,OrderSerializer
from .models import User,Categories,Item,Order
from django.http import JsonResponse



@api_view([ 'POST'])
def post(request):
    serializer = UserSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status= 301)

@api_view([ 'POST'])
def cat(request):
    serializer = CategoriesSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status= 301)

@api_view([ 'POST'])
def add(request):
    serializer = ItemSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status= 301)

@api_view([ 'POST'])
def odd(request):
    serializer = OrderSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status= 301)


@api_view(['GET'])
def getpost(request):
	x= User.objects.all()
	if x:
		serializer =UserSerializer(x, many=True)
		return Response(serializer.data)
	else:
		return Response({"Message": 'data Not Foud'})

@api_view(['GET'])
def getcat(request):
	y= Categories.objects.all()
	if y:
		serializer =CategoriesSerializer(y, many=True)
		return Response(serializer.data)
	else:
		return Response({"Message": 'data Not Foud'})

@api_view(['GET'])
def getadd(request):
	z= Item.objects.all()
	if z:
		serializer =ItemSerializer(z, many=True)
		return Response(serializer.data)
	else:
		return Response({"Message": 'data Not Foud'})



@api_view(['GET'])
def getodd(request):
	s= Order.objects.all()
	if s:
		serializer =OrderSerializer(s, many=True)
		return Response(serializer.data)
	else:
		return Response({"Message": 'data Not Foud'})


@api_view(['PUT'])
def putcat(request,_name):
    mul =Categories.objects.filter(name=request.data["name"])
    mul.update(name=request.data["name"])
    serializer = CategoriesSerializer(data=mul, many=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status= 301)


@api_view(['PUT'])
def putuse(request,_name):
    mul =User.objects.filter(name=request.data["name"])
    mul.update(name=request.data["name"])
    serializer = UserSerializer(data=mul, many=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status= 301)

@api_view(['PUT'])
def putadd(request,_name):
    mul =Categories.objects.filter(name=request.data["name"])
    mul.update(name=request.data["name"])
    serializer = CategoriesSerializer(data=mul, many=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status= 301)

@api_view(['PUT'])
def getput(request,_name):
    mul =Categories.objects.filter(name=request.data["name"])
    mul.update(name=request.data["name"])
    serializer = CategSerializer(data=mul, many=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status= 301)



@api_view(['DELETE'])
def deluse(request,_name):
        mul = User.objects.filter(name=_name)
        mul.delete()
        serializer = UserSerializer(data=mul, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status= 301)

@api_view(['DELETE'])
def delcat(request,_name):
        mul = Categories.objects.filter(name=_name)
        mul.delete()
        serializer = CategoriesSerializer(data=mul, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status= 301)
