from django.shortcuts import render
from django.shortcuts import render
from django.http import HttpResponse,HttpResponseNotFound
from shop.models import User,Categories,Item,Order
from django.db.models import Max,Avg,Min,Count,Sum
from django.db.models import Q


def getuser(request):
    use= User.objects.all()
    if use:
        context = {
            'use':use
        }
        return render(request, 'form.html', context)
    else:
        return HttpResponseNotFound("user not found")

def getcategory(request):
    cat= Categories.objects.all()
    if cat:
        context = {
            'cat':cat
        }
        return render(request, 'form.html', context)
    else:
        return HttpResponseNotFound("user not found")

def getitem(request):
    ite= Item.objects.all()
    if ite:
        context = {
            'ite':ite
        }
        return render(request, 'form.html', context)
    else:
        return HttpResponseNotFound("user not found")

def getorder(request):
    ord= Order.objects.all()
    if ord:
        context = {
            'ord':ord
        }
        return render(request, 'form.html', context)
    else:
        return HttpResponseNotFound("user not found")


def filitem(request,_name):
    _name = _name.replace('%', ' ')
    ite= Item.objects.filter(name=_name)

    if ite:
        context = {
            'ite': ite
        }
        return render(request, 'index.html', context)
    else:
        return HttpResponseNotFound("student not found")

def filorder(request,_item):
    _item = _item.replace('%', ' ')
    ord= Order.objects.filter(item=_item)

    if ord:
        context = {
            'ord': ord
        }
        return render(request, 'index.html', context)
    else:
        return HttpResponseNotFound("student not found")


def highsam(request):
    max_price = Item.objects.filter(name='samsung').aggregate(Max('price'))['price__max']
    ite = Item.objects.filter(price=max_price,name='samsung')
    if ite:
        context = {
            'ite': ite
        }
        return render(request, 'index.html', context)
    else:
        return HttpResponseNotFound("item not found")




def getaverage(request):
    average=Item.objects.all().filter(name='samsung').values('name').annotate(total=Avg('price'))
    if average:
        context={
            'average':average
        }
        return render(request,'index.html',context)
    else:
        return HttpResponseNotFound("student not found")

def gettotal(request):
    total = Item.objects.all().filter(name='samsung').values('name').annotate(total=Sum('price'))
    if total:
        context={
            'total':total
        }
        return render(request,'index.html',context)
    else:
        return HttpResponseNotFound("student not found")

def getsum(request):
    total = Order.objects.all().filter(user='').values('user').annotate(total=Sum('Total_price'))
    if total:
        context={
            'total':total
        }
        return render(request,'index.html',context)
    else:
        return HttpResponseNotFound("student not found")









from django.http import HttpResponse
from django.shortcuts import render
from .forms import CategoriesForm
from .forms import itemForm
from .forms import UserForm
from .forms import OrderForm

def get_name(request):
    form = CategoriesForm(request.POST or None)
    if form.is_valid():
        ins = form.save(commit=False)
        ins.save()
        return HttpResponse('/thanks/')
    return render(request, 'form.html', {'form': form})


def get_item(request):
    form = itemForm(request.POST or None)
    if form.is_valid():
        ins = form.save(commit=False)
        ins.save()
        return HttpResponse('/form/')
    return render(request, 'form.html', {'form': form})

def get_high(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        ins = form.save(commit=False)
        ins.save()
        return HttpResponse('/nani/')
    return render(request, 'form.html', {'form': form})


def get_low(request):
    form = OrderForm(request.POST or None)
    if form.is_valid():
        ins = form.save(commit=False)
        ins.save()
        return HttpResponse('/raju/')
    return render(request, 'form.html', {'form': form})
