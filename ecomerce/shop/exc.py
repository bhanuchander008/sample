from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render
from django.core import serializers
from .serializers import UserSerializer,CategoriesSerializer,ItemSerializer,OrderSerializer
from .models import User,Categories,Item,Order
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404

class Bhanu(APIView):
     def get_object(self, id):
         try:
             return User.objects.get(id=id)
         except User.DoesNotExist:
             raise Http404

     def get(self, request, id, format=None):
         try:
             use= self.get_object(id)
             serializer= UserSerializer(use)
             return Response(serializer.data)
         except Http404:
             return Response(" Data NOT get")

     def put(self, request, id, format=None):
         try:
             use = self.get_object(id)
             serializer = UserSerializer(use, data=request.data)
             if serializer.is_valid():
                 serializer.save()
                 return Response(serializer.data)
             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
         except Http404:
             return Response(" Data NOT UPdated")

     def delete(self, request, id, format=None):
         try:
             use = self.get_object(id)
             use.delete()
             return Response(status=status.HTTP_204_NO_CONTENT)
         except Http404:
             return Response("Data NOT DELETED")

     def post(self, request, format=None):
         try:
             serializer = UserSerializer(data=request.data)
             if serializer.is_valid():
                 serializer.save()
                 return Response(serializer.data, status=status.HTTP_201_CREATED)
             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
         except Http404:
             return Response(" Data NOT POSTED")

class Raj(APIView):
     def get_object(self, id):
         try:
             return Categories.objects.get(id=id)
         except User.DoesNotExist:
             raise Http404

     def get(self, request, id, format=None):
         try:
             use= self.get_object(id)
             serializer= CategoriesSerializer(use)
             return Response(serializer.data)
         except Http404:
             return Response(" Data NOT get")

     def put(self, request, id, format=None):
         try:
             use = self.get_object(id)
             serializer = CategoriesSerializer(use, data=request.data)
             if serializer.is_valid():
                 serializer.save()
                 return Response(serializer.data)
             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
         except Http404:
             return Response(" Data NOT UPdated")

     def delete(self, request, id, format=None):
         try:
             use = self.get_object(id)
             use.delete()
             return Response(status=status.HTTP_204_NO_CONTENT)
         except Http404:
             return Response("Data NOT DELETED")

     def post(self, request, format=None):
         try:
             serializer = CategoriesSerializer(data=request.data)
             if serializer.is_valid():
                 serializer.save()
                 return Response(serializer.data, status=status.HTTP_201_CREATED)
             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
         except Http404:
             return Response(" Data NOT POSTED")

class Sai(APIView):
     def get_object(self, id):
         try:
             return Order.objects.get(id=id)
         except Order.DoesNotExist:
             raise Http404

     def get(self, request, id, format=None):
         try:
             use= self.get_object(id)
             serializer= OrderSerializer(use)
             return Response(serializer.data)
         except Http404:
             return Response(" Data NOT get")

     def put(self, request, id, format=None):
         try:
             use = self.get_object(id)
             serializer = OrderSerializer(use, data=request.data)
             if serializer.is_valid():
                 serializer.save()
                 return Response(serializer.data)
             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
         except Http404:
             return Response(" Data NOT UPdated")

     def delete(self, request, id, format=None):
         try:
             use = self.get_object(id)
             use.delete()
             return Response(status=status.HTTP_204_NO_CONTENT)
         except Http404:
             return Response("Data NOT DELETED")

     def post(self, request, format=None):
         try:
             serializer = OrderSerializer(data=request.data)
             if serializer.is_valid():
                 serializer.save()
                 return Response(serializer.data, status=status.HTTP_201_CREATED)
             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
         except Http404:
             return Response(" Data NOT POSTED")




class Bala(APIView):
     def get_object(self, id):
         try:
             return Item.objects.get(id=id)
         except Item.DoesNotExist:
             raise Http404

     def get(self, request, id, format=None):
         try:
             use= self.get_object(id)
             serializer= ItemSerializer(use)
             return Response(serializer.data)
         except Http404:
             return Response(" Data NOT get")

     def put(self, request, id, format=None):
         try:
             use = self.get_object(id)
             serializer = ItemSerializer(use, data=request.data)
             if serializer.is_valid():
                 serializer.save()
                 return Response(serializer.data)
             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
         except Http404:
             return Response(" Data NOT UPdated")

     def delete(self, request, id, format=None):
         try:
             use = self.get_object(id)
             use.delete()
             return Response(status=status.HTTP_204_NO_CONTENT)
         except Http404:
             return Response("Data NOT DELETED")

     def post(self, request, format=None):
         try:
             serializer = ItemSerializer(data=request.data)
             if serializer.is_valid():
                 serializer.save()
                 return Response(serializer.data, status=status.HTTP_201_CREATED)
             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
         except Http404:
             return Response(" Data NOT POSTED")
