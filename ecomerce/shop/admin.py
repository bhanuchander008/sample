from django.contrib import admin

# Register your models here.
from django.contrib import admin

# Register your models here.
from .models import User,Categories,Item,Order

class UserDetail(admin.ModelAdmin):
      list_display = ('name','adress','phone','email')
admin.site.register(User,UserDetail)

class CategoriesDetail(admin.ModelAdmin):
  list_display = ('name','status')
admin.site.register(Categories,CategoriesDetail)

class ItemDetail(admin.ModelAdmin):
  list_display = ('name','Descrption','price')
admin.site.register(Item,ItemDetail)

class OrderDetail(admin.ModelAdmin):
      list_display = ('item','status','Total_price')
admin.site.register(Order,OrderDetail)
