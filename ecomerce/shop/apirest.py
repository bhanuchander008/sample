from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render
from django.core import serializers
from .serializers import UserSerializer,CategoriesSerializer,ItemSerializer,OrderSerializer
from .models import User,Categories,Item,Order
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404

class Detail(APIView):
     def get_object(self, id):
         try:
             return User.objects.get(id=id)
         except User.DoesNotExist:
             raise Http404

     def get(self, request, id, format=None):
         use= self.get_object(id)
         serializer= UserSerializer(use)
         return Response(serializer.data)

     def put(self, request, id, format=None):
         use = self.get_object(id)
         serializer = UserSerializer(use, data=request.data)
         if serializer.is_valid():
             serializer.save()
             return Response(serializer.data)
         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

     def delete(self, request, id, format=None):
         use = self.get_object(id)
         use.delete()
         return Response(status=status.HTTP_204_NO_CONTENT)

     def post(self, request, format=None):
         serializer = UserSerializer(data=request.data)
         if serializer.is_valid():
             serializer.save()
             return Response(serializer.data, status=status.HTTP_201_CREATED)
         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
