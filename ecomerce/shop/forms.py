from django import forms
from .models import Categories
from .models import Item,User,Order

class CategoriesForm(forms.ModelForm):
        class Meta:
            model = Categories
            fields = '__all__'
class itemForm(forms.ModelForm):
        class Meta:
            model = Item
            fields = '__all__'
class UserForm(forms.ModelForm):
        class Meta:
            model =User
            fields = '__all__'
class OrderForm(forms.ModelForm):
        class Meta:
            model =Order
            fields = '__all__'
