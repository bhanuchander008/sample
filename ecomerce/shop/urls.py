
from django.conf.urls import url
from .rest_api import post,cat,add,odd,getpost,getcat,getadd,getodd,getput,deluse,delcat
from .views import getuser,getcategory,getorder,getitem,filitem,filorder,highsam,getaverage,gettotal,getsum,get_name
from .views import get_item,get_high,get_low
from .api import Test1,Create,ListUsers
from .apirest import Detail
from .exc import Bhanu,Raj,Sai,Bala
urlpatterns = [
        url(r'^post/',post),
        url(r'^cat/',cat),
        url(r'^add/',add),
        url(r'^odd/',odd),
        url(r'^getpost/',getpost),
        url(r'^getcat/',getcat),
        url(r'^getadd/',getadd),
        url(r'^getodd/',getodd),
        url(r'getput/(?P<_name>[A-Za-z%]+)$',getput),
        url(r'deluse/(?P<_name>[A-Za-z%]+)$',deluse),
        url(r'delcat/(?P<_name>[/w/-]+)/$',delcat),

        #view urls
        url(r'^getuser/',getuser),
        url(r'^getcategory/',getcategory),
        url(r'^getorder/',getorder),
        url(r'^getitem/',getitem),
        url(r'filitem/(?P<_name>[A-Za-z%]+)/$',filitem),
        url(r'filorder/(?P<_item>[0-99]+)',filorder),
        url(r'^highsam/',highsam),
        url(r'^average/',getaverage),
        url(r'^total/',gettotal),
        url(r'^sum/',getsum),
        url(r'^bhanu/',get_name),
        url(r'^items/',get_item),
        url(r'^nani/',get_high),
        url(r'^raju/',get_low),


        #    class url
        url(r'^test/',Test1.as_view()),
        url(r'^users/(?P<id>[0-9]+)/$',Test1.as_view()),
        url(r'^create/',Create.as_view()),
        url(r'^list/',ListUsers.as_view()),
        url(r'^detail/(?P<id>[0-9]+)/$',Detail.as_view()),
        url(r'^tail/(?P<id>[0-9]+)/$',Bhanu.as_view()),
        url(r'^bin/(?P<id>[0-9]+)/$',Raj.as_view()),
        url(r'^din/(?P<id>[0-9]+)/$',Sai.as_view()),
        url(r'^rin/(?P<id>[0-9]+)/$',Bala.as_view()),
        #url(r'^tail/',Detail.as_view()),
]
