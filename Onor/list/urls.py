from django.conf.urls import url,include
from  list.controllers.user import getUpdateDeleteUser
from  list.controllers.postuser import postUser
from  list.controllers.carousel import getUpdateDeleteCarousel
from  list.controllers.postcarousel import postCarousel
from  list.controllers.catcarousel import getUpdateDeleteCategoriesCarousel
from  list.controllers.postcatcarousel import postCategoriesCarousel
from  list.controllers.categories import getUpdateDeleteCategories
from  list.controllers.postcategories import postCategories
from  list.controllers.listing import getUpdateDeleteListing
from  list.controllers.postlisting import postListing
from  list.controllers.listingimages import getUpdateDeleteListingImages
from  list.controllers.postlistingimages import postListingImages
from  list.controllers.postmainpagecarousel import postMainPageCarousel
from  list.controllers.mainpagecarousel import getUpdateDeleteMainPageCarousel
from  list.controllers.roles import getUpdateRoles
from  list.controllers.postroles import postRoles

urlpatterns = [
       #url(r'^post/',post),
       #url(r'^get/',get),
       #url(r'^user/(?P<id>[0-99]+)/$',getUpdateDeleteUser.as_view()),
       url(r'^postuser/',postUser.as_view()),
       url(r'^carousel/(?P<id>[0-99]+)/$',getUpdateDeleteCarousel.as_view()),
       url(r'^postcarousel/',postCarousel.as_view()),
       url(r'^catcarousel/(?P<id>[0-99]+)/$',getUpdateDeleteCategoriesCarousel.as_view()),
       url(r'^postcatcarousel/',postCategoriesCarousel.as_view()),
       url(r'^categories/(?P<id>[0-99]+)/$',getUpdateDeleteCategories.as_view()),
       url(r'^postcategories/',postCategories.as_view()),
       url(r'^listing/(?P<id>[0-99]+)/$',getUpdateDeleteListing.as_view()),
       url(r'^postlisting/',postListing.as_view()),
       url(r'^listingimages/(?P<id>[0-99]+)/$',getUpdateDeleteListingImages.as_view()),
       url(r'^postlistingimages/',postListingImages.as_view()),
       url(r'^postmainpagecarousel/',postMainPageCarousel.as_view()),
       url(r'^mainpagecarousel/',getUpdateDeleteMainPageCarousel.as_view()),
       url(r'^postRoles/',postRoles.as_view()),
       url(r'^updateRoles/',getUpdateRoles.as_view()),
       ]
