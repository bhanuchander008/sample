
from django.db import models
from django.utils.crypto import get_random_string

def file(instance, filename):
    return 'images/{0}{1}'.format(get_random_string(length=10), filename)

def file1(instance, filename):
    return 'images/{0}{1}'.format(get_random_string(length=10), filename)

def file2(instance, filename):
    return 'images/{0}{1}'.format(get_random_string(length=10), filename)

def file3(instance, filename):
    return 'images/{0}{1}'.format(get_random_string(length=10), filename)

def file4(instance, filename):
    return 'images/{0}{1}'.format(get_random_string(length=10), filename)

import crypt, getpass, pwd

'''
def login():
    password = crypt.crypt(password)
    cryptedpasswd = pwd.getpwnam(password)[1]
    if cryptedpasswd:
        if cryptedpasswd == 'x' or cryptedpasswd == '*':

            raise NotImplementedError(
                "Sorry, currently no support for shadow passwords")
        cleartext = getpass.getpass()
        return crypt.crypt(cleartext, cryptedpasswd) == cryptedpasswd
    else:
        return 1'''

class Roles(models.Model):
    name = models.CharField(max_length=50)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)

class User(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    adress =  models.CharField(max_length=50)
    email = models.EmailField(max_length=70,blank=True, null= True, unique= True)
    phone = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    status = models.BooleanField(default=True)
    role = models.ForeignKey(Roles,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
        return self.name

class MainPageCarousel(models.Model):
    name = models.CharField(max_length=50)
    status = models.BooleanField(default=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)


class Carousel(models.Model):
    image = models.ImageField(upload_to=file,blank=True,null=True)
    status = models.BooleanField(default=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    main_page = models.ForeignKey(MainPageCarousel,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)


class Categories(models.Model):
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to=file2,blank=True,null=True)
    status = models.BooleanField(default=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
        return self.name

class CategoriesCarousel(models.Model):
    image = models.ImageField(upload_to=file1,blank=True,null=True)
    status = models.BooleanField(default=True)
    categories = models.ForeignKey(Categories,on_delete=models.CASCADE)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)



class Listing(models.Model):
    name = models.CharField(max_length=50)
    body = models.CharField(max_length=50)
    phone = models.CharField(max_length=50)
    status = models.BooleanField(default=True)
    image = models.ImageField(upload_to=file3,blank=True,null=True)
    email = models.EmailField(max_length=70,blank=True, null= True, unique= True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    categories = models.ForeignKey(Categories,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
        return self.name

class ListingImages(models.Model):
   user = models.ForeignKey(User,on_delete=models.CASCADE)
   list = models.ForeignKey(Listing,on_delete=models.CASCADE)
   image = models.ImageField(upload_to=file4,blank=True,null=True)
   status = models.BooleanField(default=True)
   created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
   updated_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
