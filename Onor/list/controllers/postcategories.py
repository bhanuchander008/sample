from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render
from django.core import serializers
from list.serializers.categoriesserializer import CategoriesSerializer
from list.models import Categories
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import status
from django.http import Http404


class postCategories(APIView):
   def get(self, request,*args,**kwargs):
       user = Categories.objects.all()
       serializer = CategoriesSerializer(user, many=True)
       return Response(serializer.data)

   def post(self, request, format=None):
       serializer = CategoriesSerializer(data=request.data)
       print (serializer)
       if serializer.is_valid():
           serializer.save()
           return Response(serializer.data, status=status.HTTP_201_CREATED)
       return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
