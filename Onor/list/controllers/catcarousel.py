from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render
from django.core import serializers
from list.serializers.catcarouselserializer import CategoriesCarouselSerializer
from list.models import CategoriesCarousel
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import status
from django.http import Http404

class getUpdateDeleteCategoriesCarousel(APIView):
     def get_object(self, id):
         try:
             return CategoriesCarousel.objects.get(id=id)
         except CatCarousel.DoesNotExist:
             raise Http404

     def get(self, request, id, format=None):
         try:
             obj= self.get_object(id)
             serializer= CategoriesCarouselSerializer(obj)
             return Response(serializer.data)
         except Http404:
             return Response(" Data NOT get")

     def put(self, request, id, format=None):
         try:
             obj = self.get_object(id)
             serializer = CategoriesCarouselSerializer(obj, data=request.data)
             if serializer.is_valid():
                 serializer.save()
                 return Response(serializer.data)
             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
         except Http404:
             return Response(" Data NOT UPdated")

     def delete(self, request, id, format=None):
         try:
             obj = self.get_object(id)
             obj.delete()
             return Response(status=status.HTTP_204_NO_CONTENT)
         except Http404:
             return Response("Data NOT DELETED")
