from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render
from django.core import serializers
from list.serializers.listingimagesserializer import ListingImagesSerializer
from list.models import ListingImages
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import status
from django.http import Http404


class postListingImages(APIView):
   def get(self, request,*args,**kwargs):
       user = ListingImages.objects.all()
       serializer = ListingImagesSerializer(user, many=True)
       return Response(serializer.data)

   def post(self, request, format=None):
       serializer = ListingImageSerializer(data=request.data)
       print (serializer)
       if serializer.is_valid():
           serializer.save()
           return Response(serializer.data, status=status.HTTP_201_CREATED)
       return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
