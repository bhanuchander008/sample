from rest_framework import serializers
from list.models import ListingImages


class ListingImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ListingImages
        fields = '__all__'
