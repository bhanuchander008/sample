from rest_framework import serializers
from list.models import MainPageCarousel


class MainPageCarouselSerializer(serializers.ModelSerializer):
    class Meta:
        model = MainPageCarousel
        fields = '__all__'  
