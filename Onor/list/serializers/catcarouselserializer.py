from rest_framework import serializers
from list.models import CategoriesCarousel



class CategoriesCarouselSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoriesCarousel
        fields = '__all__' 
