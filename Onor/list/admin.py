from django.contrib import admin
from .models import User,Categories,Listing,Carousel,CategoriesCarousel,Roles,MainPageCarousel

class UserDetail(admin.ModelAdmin):
      list_display = ('first_name','last_name','email','role','password','adress','phone','status')
admin.site.register(User,UserDetail)

class RolesDetail(admin.ModelAdmin):
      list_display = ('name','status')
admin.site.register(Roles,RolesDetail)


class MainPageCarouselDetail(admin.ModelAdmin):
      list_display = ('name','status','user')
admin.site.register(MainPageCarousel,MainPageCarouselDetail)


class CarouselDetail(admin.ModelAdmin):
      list_display = ('image','status','user','main_page')
admin.site.register(Carousel,CarouselDetail)


class CategoriesCarouselDetail(admin.ModelAdmin):
      list_display = ('image','status','user','categories')
admin.site.register(CategoriesCarousel,CategoriesCarouselDetail)

class CategoriesDetail(admin.ModelAdmin):
  list_display = ('name','image','status','user')
admin.site.register(Categories,CategoriesDetail)

class ListingDetail(admin.ModelAdmin):
  list_display = ('name','image','body','phone','user','categories','status','email')
admin.site.register(Listing,ListingDetail)
